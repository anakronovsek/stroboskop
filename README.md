# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://anakronovsek@bitbucket.org/anakronovsek/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/anakronovsek/stroboskop/commits/f3bf438529c6109ce7aa5f073390cace18ab2745

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/anakronovsek/stroboskop/commits/d19798c9a5858eee0297cae2015f65f62cfba166

Naloga 6.3.2:
https://bitbucket.org/anakronovsek/stroboskop/commits/c00052cf02eb1ca60f683ce87cf97e9188b4f306

Naloga 6.3.3:
https://bitbucket.org/anakronovsek/stroboskop/commits/d52d1384637beb44da4ff88339c0c9eab7ec836c

Naloga 6.3.4:
https://bitbucket.org/anakronovsek/stroboskop/commits/d071845ca8e3ef12900c62705bdda75807386acc

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/anakronovsek/stroboskop/commits/058a90c6d03f3cb22ad37071eb92be40a4255ff0

Naloga 6.4.2:
https://bitbucket.org/anakronovsek/stroboskop/commits/463a559f5f419cee3028dc7c8c8622c8136a8978

Naloga 6.4.3:
https://bitbucket.org/anakronovsek/stroboskop/commits/28a05898f8a05564bc1f4d3065746b95f7aaedd7

Naloga 6.4.4:
https://bitbucket.org/anakronovsek/stroboskop/commits/ba6f9c7d715685baa526825108ad2e718c8ebf01